\documentclass[11pt,a4paper]{article}

%declare encodec, also spanish syntax
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[spanish,es-tabla]{babel}

%url package
\usepackage[pdftex,colorlinks=true, urlcolor=blue, linkcolor = black, citecolor=black]{hyperref}

%math fonts
\usepackage{amsmath, amsfonts, amssymb}

%graphics enviroments
\usepackage{graphicx}

%more packages <|++|>
\usepackage{csquotes}
\usepackage[backend=biber]{biblatex}
\addbibresource{biblio.bib}
\usepackage{circuitikz}
\usetikzlibrary{shapes, arrows, babel, calc, patterns, decorations.pathmorphing,decorations.markings}

%structure of the page
\usepackage[margin=1in,left=1.5in,includefoot]{geometry}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\fancyfoot[R]{\thepage\ }
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}
\begin{titlepage}
\begin{center}
\line(1,0){300} \\
[0.5cm]
\huge{\bfseries Control PID} \\ %Titulo
[0.5cm]
\textsc{\LARGE Levitador Magnético} \\ %Subtitulo
\line(1,0){200} \\
[12cm]
\end{center}
\begin{flushright}
Sistemas de control realimentado \\%Asignatura
Autores: Diego Mutizabal, \\
Exequiel Pérez, \\
Pablo Velásquez \\%Autor
Profesor: Dr. Carlos Fuhrhop %profesor
\end{flushright}
\end{titlepage}

\tableofcontents
\thispagestyle{empty}
\cleardoublepage

\setcounter{page}{1}


\begin{abstract}
		El siguiente informe ilustra los pasos seguidos en la elaboración del diseño de controlador PID del sistema electromecánico Levitador magnético como proyecto final de la asignatura sistemas de control realimentado. Se usaron los principios de la física newtoniana y electromagnética para adquirir una ecuación diferencial de segundo orden para modelar este sistema. A continuación, se usó variables de estado para reducir el orden de esta y posteriormente linealizarlo aplicando series de Taylor de primer orden. De esta expresión linealizada se obtuvo la función de transferencia, donde se usó el criterio de Routh Hurwitz para analizar la estabilidad, la cual resultó críticamente estable. Para diseñar el controlador PID del sistema se usó el método de Ziegler-Nichols. Al calcular las constantes $K_c$, $K_i$, $K_d$ se observó que el sistema efectivamente se controla, pero se necesita de un periodo exagerado de tiempo, cinco horas y media, para llegar a la respuesta deseada (escalón), mientras que, cuando se testeó el sistema con otros valores la respuesta, esta resultó mucho más rápido (menos de dos segundos). Usando estos últimos valores en el controlador PID, se sometió al sistema en conjunto a diversas perturbaciones, resultando en respuestas relativamente óptimas. Entre las conclusiones más importantes se encuentran el hecho de que la falta de experiencia en el diseño de controladores jugó en contra a la hora de controlar el sistema,y que el resultado obtenido a través del método de Ziegler-Nichols no fue óptimo para controlar el sistema en un tiempo razonable pero se logró estabilizar con valores calculados empíricamente.
\end{abstract}

\section{Introducción}

La levitación magnética, mejor conocida por su acrónimo en inglés ``Maglev'', consiste en mantener objetos suspendidos sin existir contacto mecánico por medio de un campo magnético. En la mayoría de las aplicaciones, la falta de contacto evita el desgaste y la fricción, lo que aumenta la eficiencia, reduce los costos de mantenimiento y aumenta la vida útil del sistema \cite{yaghoubi_2013}. Algunos usos de este fenómeno incluyen el transporte por levitación magnética, principalmente en trenes, rodamientos magnéticos y fundición magnética.

El objetivo principal de este proyecto es modelar teóricamente este sistema, es decir, en ausencia de una planta real (física) de levitación magnética y diseñar un controlador PID para dicho sistema utilizando los métodos de Ziegler-Nichols. Además, se pide:

\begin{itemize}
		\item Linealizar el sistema en torno a un punto de operación.
		\item Encontrar la función de transferencia del sistema y su representación en el modelo de espacio de estado.
		\item Analizar la estabilidad del sistema.
		\item Analizar la salida del sistema en lazo cerrado para una referencia escalón unitario y para una referencia variante en magnitud.
		\item Analizar la estabilidad interna del sistema. Determinar los rangos donde el sistema es estable.
		\item Analizar la respuesta y estabilidad del sistema a perturbaciones.
\end{itemize}

A pesar de que los sistemas de levitación magnética tienen un comportamiento inestable y se describen mediante ecuaciones diferenciales no lineales, la mayoría de los enfoques de diseño se basan en el modelo linealizado sobre un punto operativo nominal. Para encontrar la función de transferencia de este se usará la linealización del sistema, para posteriormente determinar su estabilidad mediante los valores propios de la matriz de variables de estados.

Una vez realizada esta labor, se utilizará el método dos de Ziegler-Nichols para calcular las constantes de proporcionalidad, integral y derivativa, ya que el primero solo aplica para sistemas estables. Cabe destacar que para simular el comportamiento del sistema se usará el programa Scilab.

\section{Modelamiento}

Nuestro sistema a modelar es un electroimán con un esfera de metal pequeña, como se ve en la figura \ref{fig:representacion}.

\begin{figure}[!h]
\centering
\begin{circuitikz}
\draw[decoration={coil, aspect=0.2, segment length=2mm, amplitude=5mm},decorate] (0,0) -- (0,3);
\draw (-0.4,0.1) rectangle (0.4,2.9);
\draw (0,0) -- (2,0) node[above] (mas) {-};
\draw (0,3) -- (2,3) node[below] (menos) {+};
\draw (2,1.5) node[] {$u(t)$};
\draw (0,-1.5) node[fill=gray,circle,minimum size=1cm] (circ) {};
\draw (0,-1.5) node[] {$m$};
\draw (circ.south) to[short] ++(0,-0.5) node[inputarrow,rotate = 270] {}
node[right] {$mg$};
\draw (circ.north) to[short] ++(0,0.5) node[inputarrow,rotate = 90] {}
node[right] {$F_m$};
\draw ($(circ) + (6,0)$) node[fill=gray,circle,minimum size = 1cm] (circ2) {};
\draw (circ2.south) to[short] ++(0,-0.5) node[inputarrow,rotate = 270] {}
node[right] {$mg$};
\draw (circ2) node[] {$m$};
\draw (circ2.north) to[short] ++(0,0.5) node[inputarrow,rotate = 90] {}
node[right] {$F_m$};
\draw (5.6,0.1) rectangle (6.4,2.9);
\draw (6,3) to[R=$R$] (6,1.5) to[L=$L$] (6,0);
\draw (8,3) to[short,i=$i(t)$] (6,3);
\draw (6,0) to[short] (8,0) node[above] {-};
\draw (8,3) node[below] {+};
\draw (8,1.5) node[] {$u(t)$};
\end{circuitikz}
\caption{Representación gráfica del sistema}
\label{fig:representacion}
\end{figure}

Nuestro sistema se modela mediante dos ecuaciones, una describe la dinámica mecánica de la interacción entre la esfera de metal y el campo magnético y la otra describe el comportamiento del circuito. La ecuación que describe el comportamiento del circuito puede obtenerse fácilmente a través de la ecuación de malla.

\[
		u(t) = i(t) R + L\frac{\mathrm{d}i(t)}{\mathrm{d}t}
\]

\begin{equation}\label{eq:electrica}
		L\frac{\mathrm{d}i(t)}{\mathrm{d}t} = u(t) - Ri(t)
\end{equation}

La ecuación de la interacción entre el campo magnético y la esfera puedes describirse utilizando la segunda ley de Newton.

\[
		\sum_{i=0}^{n}F_i = ma
\]

Para nuestro caso el eje $y$ negativo se encuentra hacia abajo por lo que la expresión $ma$ será negativa.

\begin{equation}\label{eq:mecanica}
		-mg + b\dot{y} (t) + F_m = -m \ddot{y}(t)
\end{equation}

Debemos obtener entonces una expresión que describa el campo magnético sentido por la esfera de metal. Sabemos que el trabajo realizado está dado por la expresión \cite{fawwaz}.

\begin{equation}
		W_m = \frac{1}{2} \int_{v} \vec{B} \vec{H} \mathrm{d}v
\end{equation}

Aquí cabe recordar que $\vec{B}=\mu \vec{H}$, que el campo magnético en una bobina está dado por:

\[
		\vec{B} = \mu \frac{N}{\ell} i(t)
\]

y que la inductancia de una bobina está dada por:

\[
		L = \mu \frac{N^2}{\ell} A
\]



\begin{align*}
		W_m &= \frac{1}{2} \int_{v} \vec{B} \frac{\vec{B}}{\mu} \mathrm{d}v \\
			&= \frac{1}{2} \mu \frac{N^2}{\ell^{2}} i^{2}(t)v \\
			&= \frac{1}{2} \mu \frac{N^2}{\ell^2} i^2(t) A\ell \\
			&= \frac{1}{2} \mu \frac{N^2}{\ell} A i^2(t) \\
		W_m &= \frac{1}{2} Li^2(t)
\end{align*}

Como el trabajo es la integral de la fuerza, en este caso la fuerza será  el gradiente del vector de trabajo. También debemos recordar que nuestro sistema solo tiene una componente en el eje $y$ (por simplicidad) y que el largo $\ell$ de la bobina corresponde a una distancia en el eje $y$.

\begin{align*}
		F_m &= \nabla W_m = \left(0,\frac{\partial W_m}{\partial y},0 \right) \\
			&= \hat{y} \frac{1}{2} i^2(t)\frac{\partial L(y)}{\partial y} \\
			&= -\hat{y} \frac{1}{2} i^2(t) \frac{\mu N^2 A}{y(t)^2} \\
			&= -\hat{y} \frac{1}{2} \frac{i^2(t)}{y^2(t)} \mu N^2 A \qquad ;\frac{\mu N^2 A}{2} = k \\
			&= -\hat{y} k \frac{i^2(t)}{y^2(t)}
\end{align*}

El signo negativo en este caso solo indica que la fuerza va en dirección contraria al trabajo. Reemplazando esto en la ecuación \ref{eq:mecanica}

\begin{equation}\label{eq:mecanica2}
		-mg + b\dot{y}(t) + k\left(\frac{i(t)}{y(t)} \right)^2 = -m\ddot{y}
\end{equation}

Juntando y reordenando las ecuaciones \ref{eq:electrica} y \ref{eq:mecanica2} obtenemos nuestro sistema de ecuaciones los cuales describen la planta.

\begin{equation}\label{eq:modelomecanico}
		\ddot{y} = g - \frac{b}{m} \dot{y} - \frac{k}{m}\left(\frac{i(t)}{y} \right)^2
\end{equation}

\begin{equation}\label{eq:modeloelectrico}
		\frac{\mathrm{d}i(t)}{\mathrm{d}t} = \frac{1}{L} u(t) - \frac{R}{L} i(t)
\end{equation}

\section{Modelo en espacio de estados y linealización}

Un sistema de variables de estado es la representación de un modelo físico, que representa un conjunto de entradas, salidas y variables de estado. Podemos representar el sistema de la siguiente manera:

\begin{align*}
		\begin{pmatrix}
				\dot{x}_1 \\
				\dot{x}_2 \\
				\dot{x}_3
		\end{pmatrix}
		&= A
		\begin{pmatrix}
				x_1 \\
				x_2 \\
				x_3
		\end{pmatrix}
		+ B u(t)\\
		y(t) &= C
		\begin{pmatrix}
				x_1\\
				x_2\\
				x_3
		\end{pmatrix}
		+Du(t)
\end{align*}

Donde $A$, $B$, $C$, $D$ son matrices. Mientras que $u(t)$ es la entrada del sistema e $y(t)$ es la salida del sistema, que en contexto con el sistema de levitación magnética, representan la tensión de entrada al circuito y la posición de la esfera respectivamente.

\begin{equation}\label{eq:descripciondevariables}
\begin{array}{l|l}
		x_1 = y(t) & \dot{x}_1 = x_2 = \dot{y}(t) \\
		x_2 = \dot{y}(t) & \dot{x}_2 = \ddot{y}(t) = g-\displaystyle{\frac{b}{m}x_2 - \frac{k}{m}\frac{x_{3}^{2}}{x_{1}^{2}} } \\
		x_3 = i(t) & \dot{x}_3 = \displaystyle{\frac{\mathrm{d}i(t)}{\mathrm{d}t}= -\frac{R}{L} x_3 + \frac{1}{L} u(t)}
\end{array}
\end{equation}

Para obtener la matrices $A$ y $B$ hay que linealizar el sistema y así obtener el sistema modelado en espacio de estado. Para linealizar el sistema de levitación magnética, hay que hacerlo en torno a un punto de equilibrio en el que la esfera mantenga una distancia
$x_{1Q}$ constante con respecto al electroimán, es decir, $y_Q(t)=x_{1Q}$. Por lo tanto, la variación de la velocidad y aceleración serán nulas. Es decir $\dot{x}_1 = 0$, $\dot{x}_2 = 0$. Utilizando las definiciones de la ecuación \ref{eq:descripciondevariables} y la ecuación \ref{eq:modelomecanico}, tenemos que:

\[
		\dot{x}_{2Q} = g - \frac{b}{m} x_{1Q} + \frac{k}{m} \frac{x_{3Q}}{x_{1Q}}
\]
\[
		0 = g - \frac{b}{m} x_1 + \frac{k}{m} \frac{x_3}{x_1}
\]

Si la variable $x_1$ es la posición de la esfera la cual definiremos para el punto de operación y $x_2$ es la velocidad, la cual para el punto de operación será $0$. La corriente $i(t) = x_3$ estará dada por:

\begin{equation}
i_Q = x_{3Q} = \sqrt{\frac{gmx_{1Q}^2}{K}}
\end{equation}

La expresión generalizada para un sistema en variables de estado linealizado se puede expresar como \cite{csd}:

\begin{equation}
		\Delta \vec{\dot{x}}(t) = A^{*}\Delta \vec{x}(t) + B^{*}\Delta u(t)
\end{equation}

Mientras que los coeficientes de las matrices $A^*$ y $B^*$ son el resultado del jacobiano sobre las ecuaciones $\Delta x$ en torno al punto de equilibrio, como resultado tenemos:

\begin{equation}\label{eq:matrizestadoa}
		A^* =
		\begin{pmatrix}
				0 & 1 & \\
				\frac{2kx_{3Q}^2}{mx_{1Q}^3} & -\frac{b}{m} & - \frac{2kx_{3Q}}{mx_{1Q}^2} \\
				0 & 0 & - \frac{R}{L}
		\end{pmatrix}
\end{equation}

\begin{equation}\label{eq:matrizestadob}
		B^* =
		\begin{pmatrix}
				0 \\ 0 \\ \frac{1}{L}
		\end{pmatrix}
\end{equation}

Por otra parte la salida del sistema $y(t)$ será representada por la posición $x_1(t)$:

\begin{equation}
		\Delta y(t) =
		\begin{pmatrix}
				1 & 0 & 0
		\end{pmatrix}
		\Delta \vec{x}(t) + 0\Delta u(t)
\end{equation}

Por lo que nuestro sistema en espacios de estado en torno al punto de operación queda expresado como:

\begin{equation}\label{eq:eestados}
\begin{array}{l}
		\vec{\dot{x}}(t) =
		\begin{pmatrix}
				0 & 1 & \\
				\frac{2kx_{3Q}^2}{mx_{1Q}^3} & -\frac{b}{m} & - \frac{2kx_{3Q}}{mx_{1Q}^2} \\
				0 & 0 & - \frac{R}{L}
		\end{pmatrix}
		\vec{x}(t) +
		\begin{pmatrix}
				0 \\ 0 \\ \frac{1}{L}
		\end{pmatrix}
		u(t) \\
		y(t) =
		\begin{pmatrix}
				1 & 0 & 0
		\end{pmatrix}
		\vec{x}(t)
\end{array}
\end{equation}

\section{Función de transferencia}

Otra manera útil de representar nuestro sistema es en forma de la función de transferencia. La función de transferencia está dada por la relación entre entrada y salida $y(t)/u(t)$, por conveniencia se utiliza la transformada de Laplace, por lo que trataremos de buscar $Y(s)/U(s)$ \cite{ogata}.

Aplicando la transformada de Laplace en la ecuación \ref{eq:eestados} obtenemos que:

\[
		sI\vec{X}(S) = AX(s) + BU(s)
\]

En este caso la matriz $I$ es la matriz identidad.
\[
		X(s) \left(sI-A \right) = BU(s)
\]
\[
		X(s) = \left(sI-A \right)^{-1} B U(s)
\]
\[
		Y(s) = C\left(\left(sI - A \right)^{-1} B U(s)\right)
\]

Como la función de transferencia está dada por $Y(s)/U(s)$, sustituyendo $Y(s)$ nos queda que:

\[
		G(s) = C\left(\left(sI-A \right)^{-1} B \right)
\]

Resolviendo está operación de matrices obtenemos que:

\begin{equation}
		G(s) = -\frac{2kx_{3Q}x_{1Q}}{(R + Ls)(- 2kx_{3Q}^{2} + ms^{2}x_{1Q}^{3} + bsx_{1Q}^{3})}
\end{equation}

Aquí es importante encontrar una representación numérica de la función de transferencia, para poder continuar avanzando con el control. La mayoría de estos valores han sido determinados en consideración de modelos previos del mismo experimento \cite{tesis}. Otros de estos valores requieren ser calculados teniendo en cuenta algunas constantes físicas. Uno de estos siendo la interacción entre la viscosidad del aire y la velocidad, también llamada fuerza de arrastre \cite{aire}, que como veremos, representa un valor muy pequeño en comparación con el resto. Todos los datos se pueden ver en la tabla \ref{table:valores}.

Cabe destacar que para calcular la viscosidad del aire, es necesario guiarse por la ecuación \cite{aire}:

\begin{equation}
		6\pi\eta r
\end{equation}

Al ingresar los valores para el calculo de la viscosidad, este nos arroja un valor muy pequeño, por lo cual finalmente se opto por aproximarla a cero.

\begin{equation}\label{eq:funciontransferencia}
		G(s) = \frac{3025}{s^3 +66.67s^2 + 981s + 65400}
\end{equation}

\begin{table}[!h]
\centering
\begin{tabular}{|l|c|r|}
\hline
~ & Constante & Valor \\
\hline
Viscosidad del aire & $\eta$ & $1.849\times 10^{-5}\mu \frac{kg}{ms}$ \\
\hline
Radio de la esfera & $r$ & $0.01 m$ \\
\hline
Masa de la esfera & $m$ & $0.01 kg$ \\
\hline
Resistencia de la bobina & $R$ & $1\Omega$ \\
\hline
Gravedad & $g$ & $9.81 \frac{m}{s^2}$ \\
\hline
Inductancia & $L$ & $0.015 H$ \\
\hline
Posición en equilibrio & $x_{1Q}$ & $-0.02m$ \\
\hline
Velocidad en equilibrio & $x_{2Q}$ & $0 \frac{m}{s}$ \\
\hline
Corriente en equilibrio & $x_{3Q}$ & $-0.432 A$ \\
\hline
Largo de la bobina & $\ell$ & $0.028 m$ \\
\hline
Constante magnética & $k$ & $2.1\times 10^{-4} Nm^2$\\
\hline
\end{tabular}
\caption{Valores numéricos}
\label{table:valores}
\end{table}

Cabe notar que el valor de la posición es negativo esto es dado nuestro sistema de referencia y considerando que el valor de la posición se mide desde la bobina hacia la esfera.

\section{Estabilidad del sistema}

Para saber si el sistema es estable, podemos buscar los valores propios de la matriz de estado $A$ \cite{astrom}. Para esto buscamos:

\[
		|sI-A| = 0
\]

De donde obtenemos tres raíces del polinomio característico.

\[
		s = -66.667,~s = 31.32j, ~s=-31.32j
\]

Una de las raíces del polinomio es real, mientras que dos son complejas. Como la raíz real está en el semiplano izquierdo, mientras que las raíces complejas están en el eje imaginario. El sistema es críticamente estable.


\section{Diseño de un controlador PID}

Para un controlador PID las constantes $K_p$, $k_i$ y $k_d$ se encuentran mediante el método de Ziegler-Nichols, en donde la primera constante a encontrar es la ganancia proporcional. Para tenemos que encontrar la ganancia critica del sistema, la forma para hacerlo es utilizar el método de estabilidad de Routh-Hurwitz. Se deben encontrar los valores de $s$ en que el denominador de la función de transferencia del sistema con una realimentación unitaria se haga cero.

\begin{equation}
		\frac{Y(s)}{U(s)} = \frac{KG(s)}{1+KG(s)}
\end{equation}

\[
		1 + KG(s) = 0
\]

\[
		s^3+66.67s^2+981s+65400+3025K=0
\]


Desarrollando el método de Routh-Hurwitz.

\[
		\begin{array}{c | c c}
				s^3 & 1 & 981 \\
				s^2 & 66.67 & 65400+3025K \\
				s^1 & \alpha & 0 \\
				s^0 & \beta & ~
		\end{array}
\]

Donde $\alpha$ y $\beta$ son:

\[
		\alpha = \frac{3.27 - 3025K}{66.67} \quad \beta = 65400 + 3025K
\]

Para que el sistema sea estable necesitamos que la primera columna tenga un mismo signo, vale decir que todos los valores sean positivos. Por lo que $\alpha>0$ y $\beta >0$.

Entonces el intervalo donde $\alpha$ y $\beta$ son positivos y donde la función de transferencia es estable, para un $K$ dentro del intervalo $-21.61<K<0.001$ el cual corresponde al intervalo de valores de $K$ para el cual el sistema es estable. Siendo $K_U=0.001$ la constante proporcional critica. A partir de este valor podemos obtener los valores de las constantes de $K_i$, $K_d$ y $K_p$, para esto se necesita el periodo de la oscilación. Para obtener este valor se necesita la frecuencia del sistema. La cual se obtiene a partir de la ecuación de auxiliar del método de Routh-Hurwitz.

\[
		66.67s^2+\alpha+\beta=0
\]

\[
		66.67s^2+65403.029=0
\]

Por lo que las raíces son $s = \pm 31.32j$. De esto obtenemos la frecuencia crítica $\omega_c = 31.32 ~rad/seg$. Por lo que el periodo crítico está dado por:

\[
		P_{cr} = \frac{2\pi}{\omega_c} = 0.201 s
\]

Los valores de las otras ganancias se pueden obtener como \cite{jechura}:

\begin{table}[!h]
\centering
\begin{tabular}{|l|c|c|c|}
		\hline
		Controlador & $K_p$ & $K_i$ & $K_d$ \\
		\hline
		PID Clásico & $0.6K_u$ & $1.2K_U/P_{cr}$ & $3K_UP_{cr}/40$ \\
		\hline
		Resultado & $6\times 10^{-4}$ & $5.97\times 10^{-3}$ & $1.507\times10^{-5}$ \\
		\hline
\end{tabular}
\caption{Tabla de valores PID}
\label{table:PID}
\end{table}

\section{Simulación}

Ya obtenidos los valores de control podemos simular nuestro sistema en scilab. El diagrama de control se puede ver en la figura \ref{fig:diagrama}.

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/modelo}
		\caption{Diagrama en scilab}
		\label{fig:diagrama}
\end{figure}

Al alimentar el modelo con los valores calculados, obtenemos la gráfica vista en \ref{fig:grafica}.

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/ControlPID}
		\caption{Gráfico de simulación}
		\label{fig:grafica}
\end{figure}

Se puede ver que el controlador presenta ciertos problemas, puesto que tarda al rededor de $20000$ segundos en alcanzar la respuesta de referencia, esto es un poco más de $5$ horas por lo que se hace evidente que el controlador no nos entrega un control deseable.

Por otro lado también se intentó simular el sistema para una entrada diferente, una onda senoidal. Un problema similar ocurre dado que para una frecuencia ``normal'' (entiéndase por normal una frecuencia entre $1$ y $10$ radianes por segundo) el sistema no logra estabilizarse, por lo que al optar por una frecuencia mucho menor se puede lograr una simulación más adecuada, sin embargo, nunca llegando realmente al resultado esperado. En este caso, para la simulación, se utiliza una amplitud de $0.01$ metros, una frecuencia de $0.001$ radianes por segundo, un desfase de angulo de $0$ radianes y un desfase vertical de $-0.01$ metros. El resultado de esta simulación se puede ver en el gráfico de la figura \ref{fig:entradasinreal}.

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/simentradasin}
		\caption{Simulación para una entrada senoidal}
		\label{fig:entradasinreal}
\end{figure}



No se hace evidente cual sea la razón de esta falencia. Está claro que al menos utilizando el método de Ziegler-Nichols se logra obtener un controlador del sistema. Sin embargo la lentitud de esta hace que no sea un buen sistema como par llevar a la práctica.

En las simulaciones se intentó buscar valores de control con los cuales se pueda obtener un control más rápido. Estos valores fueron obtenidos haciendo varias pruebas, pero sin fundamento teórico. Sin embargo, con estos valores $K_p = 1$, $K_i = 100$ y $K_d = 2$, se obtiene una respuesta mucho más rápida que consigue estabilizar el sistema. Esta respuesta se puede ver en la figura \ref{fig:pid}. Cabe destacar que a pesar de estos valores estar fuera del rango de estabilidad del sistema calculado, generan un sistema estable.

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/Control}
		\caption{Control con valores alternativos}
		\label{fig:pid}
\end{figure}

Basados en esta última simulación. Podemos tratar de agregar ciertas perturbaciones para observar como se comporta el sistema ante ellas. Considerando que, en un levitador magnético algunas de las perturbaciones más comunes vendrían a ser posicionar una carga por sobre el objeto levitando, este concepto se puede simular mediante la inclusión de un escalón en un tiempo aleatorio; y un campo magnético externo afectando el comportamiento del sistema, este lo hemos simulado colocando una onda senoidal en la entrada de la planta. Los diagramas de simulación para estos casos se pueden ver en la figura \ref{fig:pertur}.

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/perturbacion1}
		\includegraphics[width=0.8\textwidth]{Imagenes/perturbacionsin}
		\caption{Diagramas de simulación de perturbaciones en scilab}
		\label{fig:pertur}
\end{figure}

Para el caso de la perturbación con un escalón, se escogió como tiempo de subida $5$ segundos, mientras que la altura del escalón llegaría a los $0.01$ metros. La respuesta de está simulación se puede ver en la figura \ref{fig:simescalon}.

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/simperturbacion1}
		\caption{Simulación con un escalón en la entrada de la planta}
		\label{fig:simescalon}
\end{figure}

Por otro lado, con la perturbación senoidal, se escogió una frecuencia de $1$ radian por segundo, una amplitud de $0.02$ metros y un desfase nulo. La respuesta del sistema se puede ver en la figura \ref{fig:simsin}.
\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/simperturbacionsin}
		\caption{Simulación con un seno en la entrada de la planta}
		\label{fig:simsin}
\end{figure}

Podemos observar que el sistema se estabiliza de buena manera ante las perturbaciones. En el caso de una perturbación senoidal se ve que le cuesta más volver a estabilizarse. Solucionar este problema podría implicar otro tipo de control o algún tipo de sistema de cancelación de ruido.

Al cambiar la entrada del sistema por un seno, el controlador logra imitar la onda de entrada, sin embargo, genera un desfase con respecto a la onda original. En este caso los parámetros de entrada escogidos para la onda senoidal fueron: una frecuencia de $1$ radian por segundo, una amplitud de $0.01$ metros y un desfase horizontal de $-0.01$ metros. La respuesta se puede ver en el gráfico de la figura \ref{fig:entradasin}.

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/entradasin}
		\caption{Simulación con una entrada senoidal}
		\label{fig:entradasin}
\end{figure}

Para este tipo de entrada también se utilizaron perturbaciones de escalón y senoidal, siendo la perturbación senoidal exactamente la misma que para la entrada de escalón. Por otra parte, la perturbación de escalón cambió su magnitud a $0.03$ metros. Las respuestas a una perturbación senoidal y escalón se pueden ver en las figuras \ref{fig:entradasinpersin} y \ref{fig:entradasinperesc} respectivamente.

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/entradasinpersin}
		\caption{Simulación para una entrada senoidal con una perturbación senoidal}
		\label{fig:entradasinpersin}
\end{figure}

\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\textwidth]{Imagenes/entradasinper1}
		\caption{Simulación para entrada senoidal con una perturbación escalón}
		\label{fig:entradasinperesc}
\end{figure}


Para el caso de una perturbación senoidal la respuesta no se ve tan afectada por esta. Por otra parte para la perturbación de tipo escalón se puede ver un ``peak'' al rededor de los 5 segundos (que es el tiempo de inicio del escalón), mas este se logra estabilizar bastante rápido.

\newpage
\section{Conclusiones}

La falta de un Maglev real, con valores físicos reales medibles, y de un sensor dificultó el modelado del sistema. A pesar de que se tomaron algunos valores de otros proyectos similares como referencia, la mayoría de estos contaban con controladores prefabricados y/o sensores, los cuales, en general, eran fotodiodos, o sensores de efecto Hall. En este diseño, la salida se realimenta hacia la entrada sin un sensor de por medio, lo cual implica que la salida, posición, se igualaba al voltaje sin una transformación o equivalencia de por medio. En otras palabras, si la respuesta deseada es de $0.02$ metros, el sistema tomará esta magnitud como referencia y el sistema dependerá de $0.02$ Volts para evitar la esfera, lo cual podría explicar el exagerado tiempo que tarda en estabilizarse.

El planteamiento del sistema realizado por el equipo de trabajo no satisface apropiadamente el método de Ziegler-Nichols. Por esta razón no se logró un control satisfactorio al buscar las constantes a través de dicho método. No obstante, el sistema se logra controlar mediante el uso de variables ($K_p$, $K_i$ y $K_d$) obtenidas meramente por ensayo y error.

Una alternativa que podría facilitar el control del sistema es generar un controlador de la corriente para la bobina de manera separada. De este modo se simplifica el control del levitador.

No se logró realizar un control apropiado para el sistema de levitación magnética por algunos detalles mencionados anteriormente, este es debido a la falta de experiencia y conocimiento a la hora de construir y diseñar un sistema de control realimentado.









\printbibliography


\end{document}
